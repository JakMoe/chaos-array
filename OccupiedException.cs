﻿using System;
using System.Runtime.Serialization;

namespace My_Collections
{
    [Serializable]
    internal class OccupiedException : Exception
    {
        public OccupiedException()
        {
        }

        public OccupiedException(string message) : base(message)
        {
        }

        public OccupiedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OccupiedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}