﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace My_Collections
{
     class Program
    {
        static void Main(string[] args)
        {
            Start();
        }
        private static void Start()
        {
            ChaosArray<string> chaosArray = new ChaosArray<string>();
            string[] Items = new string[] { "Teemo", "Caitlyn", "Gragas", "Nocturne", "Jinx", "Taric" };
            for (int i = 0; i < Items.Length; i++)
            {
                try
                {
                    chaosArray.Add(Items[i]);
                }
                catch (OccupiedException)
                {
                    Console.WriteLine("Space occupied!");
                }                
            }
            Console.WriteLine("You can either GET or ADD random names to the aray: GET/ADD? ");
            string answer = Console.ReadLine().ToLower();
            if (answer == "add")
            {
                Console.WriteLine("What name do you want to add?:  ");
                string name = Console.ReadLine();
                chaosArray.Add(name);
                Start();
            }else if (answer == "get")
            {
                try
                {
                    Console.WriteLine(chaosArray.GetItem()+ " was randomly picked out of the array!");
                }
                catch (NullException)
                {
                    Console.WriteLine("No item in the random slot");
                }
            }
        }       
    }
}


    