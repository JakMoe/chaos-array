﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace My_Collections
{
    class ChaosArray<T>
    {
        public T[] chaos = new T[7];
        public static Random rnd = new Random();
        public void Add(T item)
        {
            int random = rnd.Next(chaos.Length);
            if (chaos[random] != null)
            {
                throw new OccupiedException();
            }
            else
            {
                chaos[random] = item;
                Console.WriteLine(item + " was put in as nr " + random + " in the array");
            }
        }
        public T GetItem()
        {
            int random = rnd.Next(chaos.Length);
            if(chaos[random] == null)
            {
                throw new NullException(); 
            }
            else
            {
                return (chaos[random]);
            }
        }
    }
}
